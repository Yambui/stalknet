package stalknet

import grails.gorm.transactions.Rollback
import grails.test.mixin.TestFor
import grails.test.mixin.integration.Integration
import spock.lang.Specification
import stalknet.command.MessageCommand

@Integration
@Rollback
@TestFor(MessageService)
class MessageServiceIntSpec extends Specification{

    def setup() {
        service.personService = Mock(PersonService){
            _ * getPersonSec() >> Person.findByUsername("admin")
        }
    }

    def cleanup() {
    }

    void "testMessageServiceGetAllMessages"() {
        when: "Messages stored in db"
        def testlist = service.getAllMessages()

        then:
        testlist.size() == 4
        testlist.get(0).text == "hello55"
        testlist.get(3).text == "hello"
        testlist.get(2).text == "hello1"
        testlist.get(1).text == "hello2"
    }

    void "testMessageServiceGetUserMessages"() {
        when: "username valid"
        def testlist = service.getUserMessages("admin")

        then:
        testlist.size() == 4
        testlist.get(0).text == "hello55"
        testlist.get(3).text == "hello"
        testlist.get(2).text == "hello1"
        testlist.get(1).text == "hello2"

        when: "username invalid"
        def testlist2 = service.getUserMessages("admin1")

        then:
        testlist2 == null
    }

    void "testMessageServiceSaveMessage"() {
        when: "MK setetd up"
        MessageCommand mk = new MessageCommand(text: "AAAA")
        service.saveMessage(mk)

        then:
        Message.getAll().size() == 5
        Message.findByText("AAAA")!=null
    }

    void "testMessageServiceGetUserSubsMessage"() {
        when: "Username given"
        List messagelist = service.getUserAndSubscribedMessages()

        then:
        messagelist.size() == 4
        messagelist.get(0).text == "hello55"
    }
}

