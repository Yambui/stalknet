package stalknet

import grails.test.mixin.Mock
import grails.test.mixin.TestFor
import spock.lang.Specification
import stalknet.command.MessageCommand

@TestFor(MessageController)
@Mock([Message, Person])
class MessageControllerSpec extends Specification {

    def messagelist

    def setup() {
        Person ivan = new Person(realname: "Anton", username:"Sector", password:"secret",email: "ivanow@i.ua", age:"20")
        Person admin = new Person(realname: "Ivanow Ivan", username:"admin", password:"secret", email: "ivanow@i.ua", age:"20", followed: ivan)
        messagelist = [
            new Message(text: "Ivan", creator: ivan),
            new Message(text: "test1", creator: admin),
            new Message(text: "test2", creator: admin),
            new Message(text: "test3", creator: admin)
        ]
        controller.messageService = Mock(MessageService) {
            _ * getAllMessages() >> messagelist
            _ * getUserMessages("admin") >> messagelist
            _ * getUserAndSubscribedMessages() >> messagelist
            _ * saveMessage() >> null
        }
    }

    def cleanup() {
    }

    void "testMessageControllerIndex"() {
        when: "messageListGiven"
        def list = controller.index()

        then:
        list.size() == 1
        list instanceof HashMap
        list.messages == messagelist
    }

    void "testMessageControllerSaveMessage"() {
        when: "messageComandGiven"
        MessageCommand mk = new MessageCommand(text: "TestMKtest")
        controller.saveMessage(mk)
        then:
        response.redirectedUrl == "/"
    }

    void "testMessageControllerSearchUserMsgs"() {
        when: "messageComandGiven"
        controller.params.username = "admin"
        controller.searchMsgsByUser()

        then:
        model.messages == messagelist
    }

    void "testMessageControllerGetUserAndSubscriberMessages"() {
        when: "ComandGiven"
        controller.userAndSubsMsgs()

        then:
        model.messages == messagelist
    }



}
