package stalknet

import grails.plugin.springsecurity.SpringSecurityService
import grails.test.mixin.Mock
import grails.test.mixin.TestFor
import spock.lang.Specification


@TestFor(PersonService)
@Mock([Person, Role, PersonRole])
class PersonServiceSpec extends Specification {
    Person ivan
    Person admin

    def setup() {
        admin = new Person(realname: "Ivanow Ivan", username:"admin", password:"secret", email: "ivanow@i.ua", age:"20").save()
        ivan = new Person(realname: "Anton", username:"Sector", password:"secret",email: "ivanow@i.ua", age:"20", followed: admin).save()
        Role simpleuser = new Role(authority: "ROLE_USER").save()
        Role administrator = new Role(authority: "ROLE_ADMIN").save()
        service.springSecurityService = Mock(SpringSecurityService) {
            _ * getCurrentUser() >> ivan
            _ * CurrentUser() >> ivan
        }
    }

    def cleanup() {
    }

    void "testPersonServiceGetPersonSec"() {
        when: "Messages stored in db"

        Person pers = service.getPersonSec()

        then:
        pers.username == "Sector"
    }

    void "testPersonServiceSave"() {
        when: "Person given"
        Person pers = new Person(realname: "Ivanow", username:"admin1", password:"secret", email: "ivanow@i.ua", age:"20")
        service.save(pers)
        then:
        Person.findByUsername("admin1")!=null
    }

    void "testPersonGet"() {
        when: "Id given"
        Person pers = service.get(1)
        then:
        pers!=null
    }

    void "testPersonCount"() {
        when: "Count given"
        def pers = service.count()
        then:
        pers==2
    }

    void "testPersonList"() {
        when: "List requested"
        def pers = service.list()
        then:
        pers.size()==2
        pers.get(1).username=="Sector"
    }

    void "testPersonSetUserRole"() {
        when: "role set"
        service.setUserRole(1)
        then:
        PersonRole.getAll().size()==1
        PersonRole.get(1) != null
    }

    void "testPersonDelete"() {
        when: "Delete"
        service.delete(1)
        then:
        Person.getAll().size() == 1
    }

    void "testPersonUpdate"() {
        when: "Update"
        ivan.realname = "updatetest"
        service.update(ivan)
        then:
        Person.get(2).realname == "updatetest"
    }

    void "testPersonSaveSubscriber"() {
        when: "Savesub"
        ivan.followed.remove(admin)
        service.saveSubscriber("admin")
        then:
        ivan.followed.size()==1
        ivan.followed.asList().first().username == "admin"
    }

    void "testPersonDeleteSubscriber"() {
        when: "deletesubscriber"
        service.deleteSubscriber("admin")
        then:
        ivan.followed.size()==0
    }

    void "testPersonListSubscribers"() {
        when: "listsubscribers"
        def list = service.getCurrentUserSubscribtions()
        then:
        list.size()==1
        list.first().username == "admin"
    }




}