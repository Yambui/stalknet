package stalknet

import grails.test.mixin.Mock
import grails.test.mixin.TestFor
import spock.lang.Specification
import stalknet.command.MessageCommand

@Mock([Person, Message])
@TestFor(MessageService)
class MessageServiceSpec extends Specification {

    def setup() {
        Person ivan = new Person(realname: "Anton", username:"Sector", password:"secret",email: "ivanow@i.ua", age:"20").save()
        Person admin = new Person(realname: "Ivanow Ivan", username:"admin", password:"secret", email: "ivanow@i.ua", age:"20", followed: ivan).save()
        new Message(text: "Ivan", creator: ivan).save()
        new Message(text: "test1", creator: admin).save()
        new Message(text: "test2", creator: admin).save()
        new Message(text: "test3", creator: admin).save()
        service.personService = Mock(PersonService) {
            _ * getPersonSec() >> admin
        }
    }

    def cleanup() {
    }

    void "testMessageServiceGetAllMessages"() {
        when: "Messages stored in db"
        def testlist = service.getAllMessages()

        then:
        testlist.size() == 4
        testlist.get(0).text == "Ivan"
        testlist.get(3).text == "test3"
        testlist.get(2).text == "test2"
        testlist.get(1).text == "test1"
    }

    void "testMessageServiceGetUserMessages"() {
        when: "username valid"
        def testlist = service.getUserMessages("admin")

        then:
        testlist.size() == 3
        testlist.get(2).text == "test3"
        testlist.get(1).text == "test2"
        testlist.get(0).text == "test1"

        when: "username invalid"
        def testlist2 = service.getUserMessages("admin1")

        then:
        testlist2 == null
    }

    void "testMessageServiceSaveMessage"() {
        when: "MK setetd up"
        MessageCommand mk = new MessageCommand(text: "AAAA")
        service.saveMessage(mk)

        then:
        Message.getAll().size() == 5
        Message.findByText("AAAA")!=null
    }

    void "testMessageServiceGetUserSubsMessage"() {
        when: "Username given"
        List messagelist = service.getUserAndSubscribedMessages()

        then:
        messagelist.size() == 4
        messagelist.get(0).text == "Ivan"
    }

}