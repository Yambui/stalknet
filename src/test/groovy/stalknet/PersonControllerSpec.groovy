package stalknet

import grails.test.mixin.Mock
import grails.test.mixin.TestFor
import spock.lang.Shared
import spock.lang.Specification


@TestFor(PersonController)
@Mock([Message, Person])
class PersonControllerSpec extends Specification {

    def personlist
    Person admin

    def setup() {
        Person ivan = new Person(realname: "Anton", username: "Sector", password: "secret", email: "ivanow@i.ua", age: "20").save()
        admin = new Person(realname: "Ivanow Ivan", username: "admin", password: "secret", email: "ivanow@i.ua", age: "20", followed: ivan).save()
        personlist = [ivan,admin]
        controller.personService = Mock(PersonService) {
            _ * list(_) >> personlist
            _ * count() >> 2
            _ * get(1) >> admin
            _ * getCurrentUserSubscribtions() >> personlist
        }
    }

    def cleanup() {
    }

    void "testPersonControllerIndex"() {
        when:
        controller.index()
        then:
        model.size() == 2
        model.personCount == 2
    }

    void "testPersonControllerShow"() {
        when:
        controller.show(1)

        then:
        model.person == admin
    }

    void "testPersonControllerCreate"() {
        when:
        controller.params.username = "kek"
        controller.create()

        then:
        model.person.username == "kek"
    }

    void "testPersonControllerSave"() {
        when:
        controller.request.method = "POST"
        request.format = 'form'
        controller.save(admin)

        then:
        response.redirectedUrl == '/person/index'
    }

    void "testPersonControllerSaveSub"() {
        when:
        controller.saveSubscriber()

        then:
        response.redirectedUrl == '/person/index'
    }

    void "testPersonControllerDeleteSub"() {
        when:
        controller.deleteSubscriber()

        then:
        response.redirectedUrl == '/person/subscribers'
    }

    void "testPersonControllerSubscribers"() {
        when:
        def map = controller.subscribers()

        then:
        map.messages == personlist
    }

    void "testPersonControllerAdminControler"() {
        when:
        def map = controller.adminpanel()

        then:
        map.messages == personlist
    }

    void "testPersonControllerEdit"() {
        when:
        controller.params.id = 1
        controller.edit()

        then:
        model.person == admin
    }

    void "testPersonControllerUpdate"() {
        when:
        controller.update(admin)

        then:
        model.person == admin
    }

    void "testPersonControllerNotFound"() {
        when:
        controller.notFound()

        then:
        flash.message

    }

    void "testPersonControllerDelete"() {
        when:
        request.format = 'form'
        controller.request.method = "POST"
        controller.params.username = "admin"
        controller.params.delete = "delete"
        controller.delete()
        def resp = response
        then:
        response.redirectedUrl == '/person/index'

    }


}