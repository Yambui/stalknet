// Added by the Spring Security Core plugin:
grails.plugin.springsecurity.userLookup.userDomainClassName = 'stalknet.Person'
grails.plugin.springsecurity.userLookup.authorityJoinClassName = 'stalknet.PersonRole'
grails.plugin.springsecurity.authority.className = 'stalknet.Role'
grails.plugin.databasemigration.changelogFileName = 'changelog.xml'
grails.plugin.springsecurity.logout.postOnly = false
grails.plugin.springsecurity.controllerAnnotations.staticRules = [
	[pattern: '/',               			 access: ['permitAll']],
	[pattern: '/person/create',     		 access: ['permitAll']],
	[pattern: '/person/save',     		     access: ['permitAll']],
	[pattern: '/person/index',      		 access: ['permitAll']],
	[pattern: '/person/deleteSubscriber',    access: ['ROLE_USER','ROLE_ADMIN', 'ROLE_YOUNGUSER']],
	[pattern: '/person/saveSubscriber',      access: ['ROLE_USER','ROLE_ADMIN','ROLE_YOUNGUSER']],
	[pattern: '/person/subscribers',      	 access: ['ROLE_USER','ROLE_ADMIN','ROLE_YOUNGUSER']],
	[pattern: '/message/searchMsgsByUser',   access: ['permitAll']],
	[pattern: '/message/userAndSubsMsgs',    access: ['ROLE_USER','ROLE_ADMIN','ROLE_YOUNGUSER']],
	[pattern: '/message/saveMessage',        access: ['ROLE_USER','ROLE_ADMIN','ROLE_YOUNGUSER']],
	[pattern: '/person/adminpanel',          access: ['ROLE_ADMIN']],
	[pattern: '/person/delete',              access: ['ROLE_ADMIN']],
	[pattern: '/person/edit',     		     access: ['ROLE_ADMIN']],
	[pattern: '/person/update',     		 access: ['ROLE_ADMIN']],
	[pattern: '/message/index',  access: ['permitAll']],
	[pattern: '/message/sort',   access: ['permitAll']],
	[pattern: '/error',          access: ['permitAll']],
	[pattern: '/index',          access: ['permitAll']],
	[pattern: '/index.gsp',      access: ['permitAll']],
	[pattern: '/shutdown',       access: ['permitAll']],
	[pattern: '/assets/**',      access: ['permitAll']],
	[pattern: '/**/js/**',       access: ['permitAll']],
	[pattern: '/**/css/**',      access: ['permitAll']],
	[pattern: '/**/images/**',   access: ['permitAll']],
	[pattern: '/**/favicon.ico', access: ['permitAll']]
]

grails.plugin.springsecurity.filterChain.chainMap = [
	[pattern: '/assets/**',      filters: 'none'],
	[pattern: '/**/js/**',       filters: 'none'],
	[pattern: '/**/css/**',      filters: 'none'],
	[pattern: '/**/images/**',   filters: 'none'],
	[pattern: '/**/favicon.ico', filters: 'none'],
	[pattern: '/**',             filters: 'JOINED_FILTERS']
]

