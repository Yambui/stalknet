package stalknet

import groovy.transform.EqualsAndHashCode
import groovy.transform.ToString
import grails.compiler.GrailsCompileStatic

@GrailsCompileStatic
@EqualsAndHashCode(includes = 'username')
@ToString(includes = 'username', includeNames = true, includePackage = false)
class Person implements Serializable {

    private static final long serialVersionUID = 1

    String realname
    String username
    String password
    int age
    String email
    static hasMany = [followed: Person, messages: Message]
    boolean enabled = true
    boolean accountExpired
    boolean accountLocked
    boolean passwordExpired

    Set<Role> getAuthorities() {
        (PersonRole.findAllByPerson(this) as List<PersonRole>)*.role as Set<Role>
    }

    static constraints = {
        username nullable: false, blank: false, unique: true, size: 3..15
        realname nullable: false, blank: false
        password nullable: false, blank: false, password: true
        email email: true, blank: false
        age min:10, max:100, blank:false
    }

    static mapping = {
        followed fetch: 'join'
        password column: '`password`'
        autowire true
        messages cascade: 'all-delete-orphan'
    }
}
