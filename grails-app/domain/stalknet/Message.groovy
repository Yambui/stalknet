package stalknet

class Message {

    String text
    Date dateCreated
    Person creator

    static mapping = {
        sort dateCreated:"desc"
        autowire true
    }

}
