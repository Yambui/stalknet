package stalknet

import grails.gorm.transactions.Transactional
import grails.plugin.springsecurity.SpringSecurityService
import org.springframework.beans.factory.annotation.Autowired

@Transactional
class PersonService {

    SpringSecurityService springSecurityService

    Person get(Serializable id){
        Person.findById(id)
    }

    List<Person> list(Map args){
        Person.getAll()
    }

    Long count(){
        Person.getAll().size()
    }

    void delete(Serializable id) {
        Person person = Person.get(id)
        if (!person.equals(getPersonSec())) {
            Collection<PersonRole> persrole = PersonRole.findAllByPerson(person)
            persrole*.delete(flush: true)
            person.followed = null
            person.save()
            def fol = Person.getAll().asList()
            if (fol.size() > 0) {
                fol.each { foll ->
                    if (foll.followed?.contains(person)){
                        foll.removeFromFollowed(person)
                        foll.save(flush: true)
                    }
                }
            }
            person.delete(flush: true)
        }
    }

    Person save(Person person){
        person.save(flush: true)
    }


    Person getPersonSec() {
        Person pers = springSecurityService.currentUser
        return pers
    }

    void setUserRole(Long id) {
        Person pers = Person.get(id)
        Role role
        if (pers?.age > 18) {
            role = Role.findByAuthority("ROLE_USER")
        } else {
            role = Role.findByAuthority("ROLE_YOUNGUSER")
        }
        new PersonRole(person: pers, role: role).save(flush: true, failOnError: true)
    }

    void saveSubscriber(String username) {
            Person current = getPersonSec()
            Person followed = Person.findByUsername(username)
            if (!current.equals(followed)) {
                current.followed.add(followed)
                current.save(flush: true, failOnError: true)
            }
    }

    void deleteSubscriber(String username) {
        Person current = getPersonSec()
        Person followed = Person.findByUsername(username)
        current.removeFromFollowed(followed)
        current.save(flush: true, failOnError: true)
    }

    List<Person> getCurrentUserSubscribtions() {
        Person current = getPersonSec()
        current.followed.asList()
    }
    void update(Person person){
        person.save(flush:true)
    }
}