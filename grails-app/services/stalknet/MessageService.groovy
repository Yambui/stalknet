package stalknet

import grails.converters.JSON
import grails.gorm.transactions.Transactional
import stalknet.command.MessageCommand

@Transactional
class MessageService {

    PersonService personService

    void saveMessage(MessageCommand message) {
        Message msg = new Message(text: message.text)
        Person person = personService.getPersonSec()
        msg.creator = person
        msg.save()
    }

    List<Message> getAllMessages() {
        return Message.getAll()
    }

    List<Message> getUserMessages(String username){
        Person creator = Person.findByUsername(username)
        creator ? Message.findAllByCreator(creator) : []
    }

    List<Message> getUserAndSubscribedMessages(){
        List users = personService.getPersonSec()?.followed?.asList()
        users.add(personService.getPersonSec())
        Message.findAllByCreatorInList(users)
    }

    List<Message> sortAndReturn(String id, List mlist){
        def nlist = Message.findAllByIdInList(mlist)
        if (id == "creat"){
            def sorted = nlist.toSorted { a, b -> a.creator.username <=> b.creator.username }
            return sorted
        }
        if (id == "date") {
            def sorted = nlist.toSorted { a, b -> a.dateCreated <=> b.dateCreated }
            return sorted
        }
    }
}
