<%@ page import="grails.converters.JSON" %>
<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main" />
    <title>Messages</title>

</head>
<body>
<div id="create-person" class="content scaffold-create" role="main">
    <h1 class="display-4"><b>Messages</b></h1>
    <g:if test="${flash.message}">
        <div class="errors" role="status">${flash.message}</div>
    </g:if>
    <sec:ifLoggedIn>
        <sec:ifAnyGranted roles="ROLE_ADMIN,ROLE_USER">
        <g:form url="[action: 'saveMessage']" update="messages" name="updateMessagesForm" style = "text-align: center;">
            <fieldset class="form">
                <g:textArea class="imputmessage" name="text" value=""/>
            </fieldset>
            <fieldset class="buttons">
                <g:submitButton name="create" class="btn btn-dark" value="Post" />
            </fieldset>
        </g:form>
        </sec:ifAnyGranted>
        <br>
    </sec:ifLoggedIn>

    <div class="btn-group btn-group-xs" style="margin-left: 75%">
        <button type="button" class="btn disabled" id="lbut">Sort: </button>
        <button type="button" class="btn btn-dark" id="sortcreator" onClick="${g.remoteFunction( controller:'message', params:[id:"creat", mess:messages as JSON], update: 'updatable', action:'sort' )}">Creator(Alphabetical)</button>
        <button type="button" class="btn btn-dark" id="sortdate" onClick="${g.remoteFunction( controller:'message', params:[id:"date", mess:messages as JSON], update: 'updatable', action:'sort' )}">Date(Oldest)</button>
    </div>
<div id="updatable">
    <g:render template="/templates/_fields/messagelist" var="message" collection="${messages}"/>
</div>
</div>

</body>
</html>