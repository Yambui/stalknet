    <tbody>
    <g:each in="${collection}" var="bean" status="i">
        <tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
        <g:form url="[action: 'saveSubscriber']" update="messages" name="updateMessagesForm">
                    <td><f:display bean="${bean}" property="realname" displayStyle="${displayStyle?:'table'}" theme="${theme}"/></td>
                    <td><f:display bean="${bean}" property="username" name="username" displayStyle="${displayStyle?:'table'}" theme="${theme}"/></td>
                    <g:textArea style="visibility: hidden; width: 0px; height: 0px" name="username" bean="${bean}" value="${bean.username}"/>
            <sec:ifLoggedIn>
                    <td><f:display bean="${bean}" property="email" name="email" displayStyle="${displayStyle?:'table'}" theme="${theme}"/></td>
                    <td><g:submitButton name="subscribe" class="btn btn-dark" value="Subscribe" /></td>
            </sec:ifLoggedIn>
        </g:form>
        </tr>
    </g:each>
    </tbody>
