<div class="modal-content">
    <div class="modal-header">
        <h4 class="modal-title" style="text-align: right"> <span style="position: absolute; left: 15px; top: 5px">${message.creator.realname}(${message.creator.username})</span>
            <span style="position: absolute; right: 15px; top: 5px"><g:formatDate date="${message.dateCreated}" format="HH:mm dd/MM/yyyy"/></span></h4>
    </div>
    <div class="modal-body">
        <p style="text-align: center"><h1>${message.text}</h1></p>
    </div>
</div>
<br>