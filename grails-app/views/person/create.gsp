<!DOCTYPE html>
<html>
    <head>
        <meta name="layout" content="main" />
        <g:set var="entityName" value="${message(code: 'person.label', default: 'Person')}" />
        <title><g:message code="default.create.label" args="[entityName]" /></title>
    </head>
    <body>
        <div id="create-person" class="content scaffold-create" role="main">
            <h1 class="display-4"><b>Register</b></h1>
            <g:if test="${flash.message}">
            <div class="message" role="status">${flash.message}</div>
            </g:if>
            <g:hasErrors bean="${this.person}">
            <ul class="errors" role="alert">
                <g:eachError bean="${this.person}" var="error">
                <li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message error="${error}"/></li>
                </g:eachError>
            </ul>
            </g:hasErrors>
            <g:form resource="${this.person}" method="POST" style = "text-align: center;">
                <fieldset class="form">
                    <f:all bean="person" except="followed, enabled, accountExpired, accountLocked, passwordExpired, messages"/>
                </fieldset>
                <fieldset class="buttons">
                     <g:submitButton name="create" class="btn btn-dark" value="Register" />
                </fieldset>
            </g:form>
        </div>
    </body>
</html>
