<!DOCTYPE html>
<html>
    <head>
        <meta name="layout" content="main" />
        <g:set var="entityName" value="${message(code: 'person.label', default: 'Person')}" />
        <title><g:message code="default.list.label" args="[entityName]" /></title>
    </head>
    <body>
        <div id="list-person" class="content scaffold-list" role="main">
            <h1 class="display-4"><b>Edit users</b></h1>
                <g:if test="${flash.message}">
                <div class="message" role="status">${flash.message}</div>
                </g:if>
            <table>
                <thead>
                <tr>
                    <g:sortableColumn property="realname" title="Real name"/>
                    <g:sortableColumn property="username" title="Nickname"/>
                    <sec:ifLoggedIn>
                    <td>Delete</td>
                    </sec:ifLoggedIn>
                </tr>
                </thead>
                <tbody>
                <g:render template="/templates/_fields/deleteuser" var="collection" collection="${messages}"/>
                </tbody>
            </table>
                <div class="pagination">
                <g:paginate total="${personCount ?:0}" />
            </div>
        </div>
    </body>
</html>