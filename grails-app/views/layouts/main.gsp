<!doctype html>
<html lang="en" class="no-js">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <title>
        <g:layoutTitle default="Stalknet"/>
    </title>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js"></script>
    <asset:stylesheet src="application.css"/>
    <g:layoutHead/>
</head>

<body>
<div class="jumbotron-fluid text-center" style="margin-top:0">
    <h1 class="display-3"><b>STALKNET</b></h1>
</div>

<nav class="navbar navbar-expand-lg bg-dark navbar-dark">
    <div class="collapse navbar-collapse" id="collapsibleNavbar">
        <ul class="navbar-nav">
            <li class="nav-item">
                <a class="nav-link" href="/message/index">Messages</a>
            </li>
            <sec:ifNotLoggedIn>
                <li class="nav-item">
                    <a class="nav-link" href="/login">Login</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="/person/create">Register</a>
                </li>
            </sec:ifNotLoggedIn>
            <sec:ifLoggedIn>
                <li class="nav-item">
                    <a class="nav-link" href="/message/userAndSubsMsgs">Watch Feed</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="/person/subscribers">Subscription</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="/logout">Logout</a>
                </li>
            </sec:ifLoggedIn>
            <li class="nav-item">
                <a class="nav-link" href="/person/index">User List</a>
            </li>
            <sec:ifAnyGranted roles="ROLE_ADMIN">
                <li class="nav-item">
                    <a class="nav-link" href="/person/adminpanel">Admin Panel</a>
                </li>
            </sec:ifAnyGranted>
            <g:form class="form-inline my-2 my-lg-0" style="position:absolute; right: 20px; top: 30%"
                    url="[action: 'searchMsgsByUser', controller: 'message']">
                <input class="form-control mr-sm-2" type="text" name="username" placeholder="Username">
                <button class="btn btn-secondary my-2 my-sm-0" type="submit">Search Messages</button>
            </g:form>
        </ul>
    </div>
</nav>

<g:layoutBody/>

<div class="footer" role="contentinfo">
    <p>

    <h1 class="display-5">(c)Technology</h1></p>
</div>
<asset:javascript src="application.js"/>
</body>
</html>
