package stalknet

class BootStrap {


    def init = { servletContext ->
        Person admin = new Person(realname: "Ivanow Ivan", username:"admin", password:"secret", email: "ivanow@i.ua", age:"20").save()
        Person ivan = new Person(realname: "Anton", username:"Sector", password:"secret",email: "ivanow@i.ua", age:"20").save()
        Message mess = new Message(text: "hello", creator: admin).save()
        Message mess1 = new Message(text: "hello1", creator: admin).save()
        Message mess4 = new Message(text: "hello ivan", creator: ivan).save()
        Message mess2 = new Message(text: "hello2", creator: admin).save()
        Message mess3 = new Message(text: "hello55", creator: admin).save()
        Message mess5 = new Message(text: "hello ivan1", creator: ivan).save()
        Role simpleuser = new Role(authority: "ROLE_USER").save()
        Role simpleuser2 = new Role(authority: "ROLE_YOUNGUSER").save()
        Role administrator = new Role(authority: "ROLE_ADMIN").save()
        simpleuser.save()
        PersonRole.create (admin, administrator)
        PersonRole.create(ivan, simpleuser)
        PersonRole.withSession {
            it.flush()
            it.clear()
        }
    }
    def destroy = {
    }
}
