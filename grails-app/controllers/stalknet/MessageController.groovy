package stalknet

import grails.converters.JSON
import org.grails.web.json.JSONObject
import stalknet.command.MessageCommand

class MessageController {

    MessageService messageService

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index () {
        def messageList = messageService.getAllMessages()
        return [messages:messageList]
    }


    def saveMessage (MessageCommand mk) {
        if (mk.hasErrors()) {
            request.withFormat {
                form multipartForm {
                    flash.message = message(code:'toLongMessage')
                }
            }
        } else {
            messageService.saveMessage(mk)
        }
        redirect (controller: 'message', action: 'index')
    }

    def searchMsgsByUser(){
        def userMessageList = messageService.getUserMessages(params.username)
        Message error = new Message(creator: new Person(username: "Administrator", realname: "Administrator", password: "not important"),
                text: "User ${params.username} has no messages", dateCreated: new Date())
        Map<String,List> modelMap = [:]
        if ((userMessageList == null)||(userMessageList.size()==0)){
            modelMap = [messages: [error]]
        } else {
            modelMap = [messages: userMessageList]
        }
        return render(view: "index", model: modelMap)
    }

    def userAndSubsMsgs(){
        def userMessageList = messageService.getUserAndSubscribedMessages()
        println(userMessageList.toString())
        Message error = new Message(creator: new Person(username: "Administrator", realname: "Administrator", password: "not important"),
                text: "No messages to show", dateCreated: new Date())
        if (userMessageList == null){
            return render(view: "index", model: [messages: [error]])
        } else {
            return render(view: "index", model: [messages: userMessageList])
        }
    }

    def sort(){
        List<JSONObject> mlist = JSON.parse(params.mess) as List
        List<Integer> messagesid = mlist.collect {
            it.getInt("id")
        }
        def list = messageService.sortAndReturn(params.id, messagesid)
        render (template: "/templates/_fields/messagelist", var: "message", collection: list)
    }
}
