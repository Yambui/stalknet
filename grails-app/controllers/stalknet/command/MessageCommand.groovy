package stalknet.command

import  grails.validation.Validateable

class MessageCommand implements Validateable {
    String text

    static constraints = {
        text(blank: false, maxSize: 140)
    }
}