package stalknet

import grails.validation.ValidationException
import static org.springframework.http.HttpStatus.*

class PersonController {

    PersonService personService

    static allowedMethods = [save: "POST", delete: "POST"]

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond personService.list(params), model: [personCount: personService.count()]
    }

    def show(Long id) {
        respond personService.get(id)
    }

    def create() {
        respond new Person(params)
    }

    def save(Person person) {
        if (person == null) {
            notFound()
            return
        }

        try {
            personService.save(person)
            personService.setUserRole(person.id)
            redirect(controller: "person", action: "index")
        } catch (ValidationException e) {
            respond person.errors, view: 'create'
            return
        }
    }

    def saveSubscriber() {
        personService.saveSubscriber(params.username)
        redirect(actionName: "index")
    }

    def deleteSubscriber() {
        personService.deleteSubscriber(params.username)
        redirect(controller: "person", action: "subscribers")
    }

    def subscribers() {
        def sublist = personService.getCurrentUserSubscribtions()
        return [messages: sublist]
    }

    def adminpanel() {
        def sublist = personService.list()
        return [messages: sublist]
    }


    def edit() {
        respond personService.get(params.id)
    }

    def update(Person person) {
        if (person == null) {
            notFound()
            return
        }
        try {
            personService.update(person)
        } catch (ValidationException e) {
            respond person.errors, view: 'edit'
            return
        }

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'person.label', default: 'Person'), person.username])
                redirect (controller: 'person', action: 'index')
            }
            '*' { respond person, [status: OK] }
        }
    }

    def delete() {
        if (params.username == null) {
            notFound()
            return
        }
        Person pers = Person.findByUsername(params.username)
        if (params.keySet().contains("delete")) {

            personService.delete(pers.id)

            request.withFormat {
                form multipartForm {
                    flash.message = message(code: 'default.deleted.message', args: [message(code: 'person.label', default: 'Person'), pers.username])
                  redirect action: "index", method: "GET"

                }
                '*' { render status: NO_CONTENT }
            }
        }
        else {
            redirect(action: "edit", params: [id: pers.id])
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'person.label', default: 'Person'), params.id])
                redirect action: "index", method: "GET"
            }
        }
    }
}
